# Go Parser

### Запуск
```bash
git clone https://gitlab.com/dvkgroup/go-parser.git
git clone https://gitlab.com/dvkgroup/userservice.git
cd go-parser
docker compose up
```

Парсер находится на порту 8080 [parser](http://localhost:8080/swagger) и работает с сервером авторизации по Rest API

Авторизация на порту 8081 [auth&user](http://localhost:8081/swagger)
