module gitlab.com/dvkgroup/go-parser

go 1.19

//replace gitlab.com/dvkgroup/userservice => ../userservice

require (
	github.com/PuerkitoBio/goquery v1.8.1
	github.com/go-chi/chi v1.5.4
	github.com/golang-jwt/jwt/v4 v4.5.0
	github.com/jmoiron/sqlx v1.3.5
	github.com/joho/godotenv v1.5.1
	github.com/lib/pq v1.10.9
	github.com/tebeka/selenium v0.9.9
	go.mongodb.org/mongo-driver v1.11.7
	go.uber.org/zap v1.24.0
)

require (
	github.com/andybalholm/cascadia v1.3.1 // indirect
	github.com/blang/semver v3.5.1+incompatible // indirect
	github.com/go-sql-driver/mysql v1.7.1 // indirect
	github.com/golang/snappy v0.0.3 // indirect
	github.com/klauspost/compress v1.13.6 // indirect
	github.com/montanaflynn/stats v0.0.0-20171201202039-1bf9dbcd8cbe // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/xdg-go/pbkdf2 v1.0.0 // indirect
	github.com/xdg-go/scram v1.1.1 // indirect
	github.com/xdg-go/stringprep v1.0.3 // indirect
	github.com/youmark/pkcs8 v0.0.0-20181117223130-1be2e3e5546d // indirect
	go.uber.org/atomic v1.7.0 // indirect
	go.uber.org/multierr v1.6.0 // indirect
	golang.org/x/crypto v0.9.0 // indirect
	golang.org/x/net v0.10.0 // indirect
	golang.org/x/sync v0.2.0 // indirect
	golang.org/x/text v0.9.0 // indirect
)
