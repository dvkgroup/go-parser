package api

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"gitlab.com/dvkgroup/go-parser/config"
	"gitlab.com/dvkgroup/go-parser/internal/controller"
	"gitlab.com/dvkgroup/go-parser/internal/parser"
	"gitlab.com/dvkgroup/go-parser/internal/repository/sqlDB"
	"gitlab.com/dvkgroup/go-parser/internal/restapi"
	"gitlab.com/dvkgroup/go-parser/internal/usercase"
	"net/url"
)

func Run(conf config.AppConf, db *sqlx.DB) {
	p := parser.NewVacancyParser(conf)

	rSQL := sqlDB.NewVacancyStorage(db)
	vs := usercase.NewVacancyService(rSQL, p)

	vc := controller.NewVacancyController(vs)

	restapiURL := fmt.Sprintf("%s:%s", conf.UserService.Host, conf.UserService.Port)
	rc := restapi.NewClient(&url.URL{Scheme: "http", Host: restapiURL}, nil)

	us := usercase.NewUserService(rc)
	uc := controller.NewUserController(us, conf)

	as := usercase.NewAuthService(rc)
	ac := controller.NewAuthController(as, conf)

	srv := controller.NewServerController(vc, uc, ac, conf)

	srv.Start()
}
