package controller

import (
	"encoding/json"
	"gitlab.com/dvkgroup/go-parser/config"
	"gitlab.com/dvkgroup/go-parser/internal/entity/auth"
	"net/http"
)

type AuthUseCase interface {
	Register(token string, in auth.RegisterRequest) (auth.RegisterResponse, error)
	Login(token string, in auth.LoginRequest) (auth.AuthResponse, error)
	Refresh(token string) (auth.AuthResponse, error)
	Verify(token string, in auth.VerifyRequest) (auth.VerifyResponse, error)
}

type AuthController struct {
	a    AuthUseCase
	conf config.AppConf
}

func NewAuthController(a AuthUseCase, conf config.AppConf) *AuthController {
	return &AuthController{a: a, conf: conf}
}

func (c AuthController) Register(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get(authorization)

	var req auth.RegisterRequest

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	p, err := c.a.Register(token, req)
	if err != nil {
		writeResponse(w, auth.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data:      auth.Data{},
		})
		return
	}

	writeResponse(w, auth.RegisterResponse{
		Success:   p.Success,
		ErrorCode: p.ErrorCode,
		Data:      p.Data,
	})
}

func (c AuthController) Login(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get(authorization)

	var req auth.LoginRequest

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	p, err := c.a.Login(token, req)
	if err != nil {
		writeResponse(w, auth.AuthResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data:      auth.LoginData{},
		})
		return
	}

	writeResponse(w, auth.AuthResponse{
		Success:   p.Success,
		ErrorCode: p.ErrorCode,
		Data:      p.Data,
	})
}

func (c AuthController) Refresh(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get(authorization)

	p, err := c.a.Refresh(token)
	if err != nil {
		writeResponse(w, auth.AuthResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data:      auth.LoginData{},
		})
		return
	}

	writeResponse(w, auth.AuthResponse{
		Success:   p.Success,
		ErrorCode: p.ErrorCode,
		Data:      p.Data,
	})
}

func (c AuthController) Verify(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get(authorization)

	var req auth.VerifyRequest

	err := json.NewDecoder(r.Body).Decode(&req)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	p, err := c.a.Verify(token, req)
	if err != nil {
		writeResponse(w, auth.VerifyResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data:      auth.Data{},
		})
		return
	}

	writeResponse(w, auth.VerifyResponse{
		Success:   p.Success,
		ErrorCode: p.ErrorCode,
		Data:      p.Data,
	})
}
