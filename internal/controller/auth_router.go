package controller

import (
	"github.com/go-chi/chi"
	"net/http"
)

func authRouter(c *AuthController, s *ServerController) http.Handler {
	r := chi.NewRouter()

	r.Post("/register", c.Register)
	r.Post("/login", c.Login)
	r.Post("/verify", c.Verify)

	r.Route("/refresh", func(r chi.Router) {
		r.Use(s.CheckRefreshToken)
		r.Post("/", c.Refresh)
	})

	return r
}
