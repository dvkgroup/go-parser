package controller

import (
	"context"
	"fmt"
	"github.com/go-chi/chi"
	"github.com/go-chi/chi/middleware"
	"github.com/golang-jwt/jwt/v4"
	"log"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"time"

	"gitlab.com/dvkgroup/go-parser/config"
)

type ServerController struct {
	v    *VacancyController
	u    *UserController
	a    *AuthController
	conf config.AppConf
}

func NewServerController(v *VacancyController, u *UserController, a *AuthController, conf config.AppConf) *ServerController {
	return &ServerController{v: v, u: u, a: a, conf: conf}
}

func (c *ServerController) Start() {
	r := chi.NewRouter()
	r.Use(middleware.Logger)

	r.Get("/public/*", func(w http.ResponseWriter, r *http.Request) {
		http.StripPrefix("/public/", http.FileServer(http.Dir("./public"))).ServeHTTP(w, r)
	})

	r.Mount("/swagger", swaggerRouter())
	r.Mount("/vacancies", vacancyRouter(c.v, c))
	r.Mount("/user", userRouter(c.u, c))
	r.Mount("/auth", authRouter(c.a, c))

	port := fmt.Sprintf(":%s", c.conf.Server.Port)

	srv := &http.Server{
		Addr:    port,
		Handler: r,
	}

	// Запуск веб-сервера в отдельном горутине
	go func() {
		log.Printf("server started on port %s\n", c.conf.Server.Port)
		if err := srv.ListenAndServe(); err != nil && err != http.ErrServerClosed {
			log.Fatalf("listen: %s\n", err)
		}
	}()

	// Ожидание сигнала для начала завершения работы
	quit := make(chan os.Signal, 1)
	signal.Notify(quit, os.Interrupt)
	<-quit
	log.Println("Shutdown Server ...")

	// Установка тайм-аута для завершения работы
	ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()
	if err := srv.Shutdown(ctx); err != nil {
		log.Fatal("Server Shutdown:", err)
	}

	log.Println("Server exiting")
}

func (c *ServerController) CheckAccessToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenRaw := r.Header.Get(authorization)
		tokenParts := strings.Split(tokenRaw, " ")
		if len(tokenParts) < 2 && tokenParts[0] != "Bearer" {
			errorWrite(w, fmt.Errorf("bad jwt-token"))
			return
		}

		token, err := jwt.Parse(tokenParts[1], func(token *jwt.Token) (interface{}, error) {
			return []byte(c.conf.Token.AccessSecret), nil
		})
		if err != nil {
			errorWrite(w, err)
			return
		}

		if !token.Valid {
			errorWrite(w, fmt.Errorf("not valid jwt-token"))
			return
		}

		next.ServeHTTP(w, r)
	})
}

func (c *ServerController) CheckRefreshToken(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		tokenRaw := r.Header.Get(authorization)
		tokenParts := strings.Split(tokenRaw, " ")
		if len(tokenParts) < 2 && tokenParts[0] != "Bearer" {
			errorWrite(w, fmt.Errorf("bad jwt-token"))
			return
		}

		token, err := jwt.Parse(tokenParts[1], func(token *jwt.Token) (interface{}, error) {
			return []byte(c.conf.Token.RefreshSecret), nil
		})
		if err != nil {
			errorWrite(w, err)
			return
		}

		if !token.Valid {
			errorWrite(w, fmt.Errorf("not valid jwt-token"))
			return
		}

		next.ServeHTTP(w, r)
	})
}

func errorWrite(w http.ResponseWriter, err error) {
	w.Header().Set("Content-Type", "text/plain;charset=utf-8")
	w.WriteHeader(http.StatusForbidden)
	w.Write([]byte(err.Error()))
}
