package controller

import (
	"net/http"

	"github.com/go-chi/chi"
)

func swaggerRouter() http.Handler {
	r := chi.NewRouter()
	r.Get("/", SwaggerUI)

	return r
}
