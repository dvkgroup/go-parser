package controller

import (
	"encoding/json"
	"gitlab.com/dvkgroup/go-parser/config"
	"gitlab.com/dvkgroup/go-parser/internal/entity/user"
	"net/http"
)

type UserUseCase interface {
	Profile(token string) (user.ProfileResponse, error)
	ChangePassword(token string, cpr user.ChangePasswordRequest) (user.ChangePasswordResponse, error)
}

type UserController struct {
	u    UserUseCase
	conf config.AppConf
}

func NewUserController(u UserUseCase, conf config.AppConf) *UserController {
	return &UserController{u: u, conf: conf}
}

func (c UserController) Profile(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get(authorization)

	p, err := c.u.Profile(token)
	if err != nil {
		writeResponse(w, user.ProfileResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data:      user.Data{},
		})
		return
	}

	writeResponse(w, user.ProfileResponse{
		Success:   p.Success,
		ErrorCode: p.ErrorCode,
		Data: user.Data{
			Message: p.Data.Message,
			User:    p.Data.User,
		},
	})
}

func (c UserController) ChangePassword(w http.ResponseWriter, r *http.Request) {
	token := r.Header.Get(authorization)

	var crp user.ChangePasswordRequest

	err := json.NewDecoder(r.Body).Decode(&crp)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
	defer r.Body.Close()

	p, err := c.u.ChangePassword(token, crp)
	if err != nil {
		writeResponse(w, user.ChangePasswordResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Message:   "error change password",
		})
		return
	}

	writeResponse(w, user.ChangePasswordResponse{
		Success:   p.Success,
		ErrorCode: p.ErrorCode,
		Message:   p.Message,
	})
}

func writeResponse(w http.ResponseWriter, data interface{}) {
	// выставляем заголовки, что отправляем json в utf8
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(data)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
