package controller

import (
	"github.com/go-chi/chi"
	"net/http"
)

const authorization = "Authorization"

func userRouter(c *UserController, s *ServerController) http.Handler {
	r := chi.NewRouter()

	r.Use(s.CheckAccessToken)

	r.Get("/profile", c.Profile)
	r.Post("/chpass", c.ChangePassword)

	return r
}
