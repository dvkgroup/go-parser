package controller

import (
	"encoding/json"
	"fmt"
	"gitlab.com/dvkgroup/go-parser/internal/entity"
	"net/http"
	"strconv"
)

type VacancyUseCase interface {
	Search(string) int
	GetByID(int64) (entity.Vacancy, error)
	GetList() []entity.Vacancy
	Delete(int64) error
	Update(vacancy entity.Vacancy) (entity.Vacancy, error)
}

type VacancyController struct {
	u VacancyUseCase
}

func NewVacancyController(u VacancyUseCase) *VacancyController {
	return &VacancyController{u: u}
}

func (c VacancyController) Search(w http.ResponseWriter, r *http.Request) {
	query := r.FormValue("query")

	count := c.u.Search(query)

	_, _ = fmt.Fprintf(w, "Find %d vacancies.\n", count)
}

func (c VacancyController) GetByID(w http.ResponseWriter, r *http.Request) {
	vacancyID, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	vacancy, err := c.u.GetByID(vacancyID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	writeVacancyResponse(w, vacancy)
}

func (c VacancyController) GetList(w http.ResponseWriter, r *http.Request) {
	vacancies := c.u.GetList()
	writeVacanciesResponse(w, vacancies)
}

func (c VacancyController) Delete(w http.ResponseWriter, r *http.Request) {
	vacancyID, err := strconv.ParseInt(r.FormValue("id"), 10, 64)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	err = c.u.Delete(vacancyID)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}
}

func (c VacancyController) Update(w http.ResponseWriter, r *http.Request) {
	var vacancy entity.Vacancy

	err := json.NewDecoder(r.Body).Decode(&vacancy) // считываем приходящий json из *http.Request в структуру Pet
	if err != nil {                                 // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}
	defer r.Body.Close()

	vacancy, err = c.u.Update(vacancy)
	if err != nil { // в случае ошибки отправляем ошибку Bad request code 400
		http.Error(w, err.Error(), http.StatusBadRequest)
		return // не забываем прекратить обработку нашего handler (ручки)
	}

	writeVacancyResponse(w, vacancy)
}

func writeVacancyResponse(w http.ResponseWriter, vacancy entity.Vacancy) {
	// выставляем заголовки, что отправляем json в utf8
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(vacancy)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

func writeVacanciesResponse(w http.ResponseWriter, vacancies []entity.Vacancy) {
	// выставляем заголовки, что отправляем json в utf8
	w.Header().Set("Content-Type", "application/json;charset=utf-8")
	err := json.NewEncoder(w).Encode(vacancies)

	if err != nil { // отправляем 500 ошибку в случае неудачи
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}
