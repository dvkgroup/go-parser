package controller

import (
	"net/http"

	"github.com/go-chi/chi"
)

func vacancyRouter(c *VacancyController, s *ServerController) http.Handler {
	r := chi.NewRouter()

	r.Use(s.CheckAccessToken)

	r.Post("/search", c.Search)
	r.Post("/get", c.GetByID)
	r.Post("/delete", c.Delete)
	r.Get("/list", c.GetList)
	r.Put("/update", c.Update)

	return r
}
