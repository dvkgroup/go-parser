package docs

import (
	"gitlab.com/dvkgroup/go-parser/internal/entity/auth"
)

//go:generate swagger generate spec -o ../../public/swagger.json --scan-models

// swagger:route POST /auth/register auth registerRequest
// Регистрация пользователя.
// responses:
//   200: registerResponse

// swagger:parameters registerRequest
type registerRequest struct {
	// in:body
	Body auth.RegisterRequest
}

// swagger:response registerResponse
type registerResponse struct {
	// in:body
	Body auth.RegisterResponse
}

// swagger:route POST /auth/login auth loginRequest
// Авторизация пользователя.
// responses:
//   200: loginResponse

// swagger:parameters loginRequest
type loginRequest struct {
	// in:body
	Body auth.LoginRequest
}

// swagger:response loginResponse
type loginResponse struct {
	// in:body
	Body auth.AuthResponse
}

// swagger:route POST /auth/refresh auth refreshRequest
// Обновление рефреш токена.
// security:
//   - Bearer: []
// responses:
//   200: refreshResponse

// swagger:response refreshResponse
type refreshRespone struct {
	// in:body
	Body auth.AuthResponse
}

// swagger:route POST /auth/verify auth verifyRequest
// Верификация почты/телефона пользователя.
// responses:
//   200: verifyResponse

// swagger:parameters verifyRequest
type verifyRequest struct {
	// in:body
	Body auth.VerifyRequest
}

// swagger:response verifyResponse
type verifyResponse struct {
	// in:body
	Body auth.AuthResponse
}
