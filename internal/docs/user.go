package docs

import (
	"gitlab.com/dvkgroup/go-parser/internal/entity/user"
)

//go:generate swagger generate spec -o ../../public/swagger.json --scan-models

// swagger:route GET /user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
//   - Bearer: []
// responses:
// 200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in: body
	Body user.ProfileResponse
}

// swagger:route POST /user/chpass user chpassRequest
// Смена пароля.
// security:
//   - Bearer: []
// responses:
// 200: chpassResponse

// swagger:parameters chpassRequest
type chpassRequest struct {
	// in: body
	Body user.ChangePasswordRequest
}

// swagger:response chpassResponse
type chpassResponse struct {
	// in: body
	Body user.ChangePasswordResponse
}
