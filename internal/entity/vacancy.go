package entity

import (
	"encoding/json"
	"log"
	"strings"
	"time"
)

type Vacancy struct {
	Title          string      `json:"title" db:"title"`
	Description    string      `json:"description" db:"description"`
	EmploymentType string      `json:"employmentType" db:"employment_type"`
	DatePosted     VacancyDate `json:"datePosted" db:"date_posted"`
	ValidThrough   VacancyDate `json:"validThrough" db:"valid_through"`
	Identifier     Identifier  `json:"identifier" db:"identifier"`
	JobLocation    JobLocation `json:"jobLocation" db:"job_location"`
}

type Identifier struct {
	Type  string `json:"@type" db:"type"`
	Name  string `json:"name" db:"name"`
	Value string `json:"value" db:"id"`
}

type JobLocation struct {
	Address string `json:"address" db:"address"`
}

func (l *JobLocation) UnmarshalJSON(data []byte) (err error) {
	if string(data) == "null" || string(data) == "" {
		return
	}

	type tempData1 struct {
		Address string
	}

	type tempData2 struct {
		Address struct {
			AddressLocality string
		}
	}

	l.Address = ""

	//"jobLocation": {
	//      "@type": "Place",
	//      "address": "Russia"
	//    },
	var jl1 tempData1
	if err = json.Unmarshal(data, &jl1); err == nil {
		l.Address = jl1.Address
		return
	}

	//    "jobLocation": {
	//      "@type": "Place",
	//      "address": {
	//        "@type": "PostalAddress",
	//        "streetAddress": "ул. Луговая, д. 4, корп. 5",
	//        "addressLocality": "Москва",
	//        "addressCountry": {
	//          "@type": "Country",
	//          "name": "Россия"
	//        }
	//      }
	//     },
	var jl2 tempData2
	if err = json.Unmarshal(data, &jl2); err == nil {
		l.Address = jl2.Address.AddressLocality
		return
	}

	//  "jobLocation": [
	//      {"@type": "Place","address": "Барнаул"},
	//      {"@type": "Place","address": "Новосибирск"},
	//      {"@type": "Place","address": "Хабаровск"}
	//  ],
	var jlList []tempData1
	err = json.Unmarshal(data, &jlList)
	if err != nil {
		log.Println(err)
		return
	}

	for i := range jlList {
		l.Address += jlList[i].Address + " "
	}

	return
}

type VacancyDate struct {
	//time.Time
	Date string `json:"date" db:"date"`
}

func (d *VacancyDate) UnmarshalJSON(data []byte) (err error) {
	format := "2006-01-02"

	s := strings.Trim(string(data), "\"") // remove quotes
	if s == "null" {
		return
	}

	t, _ := time.Parse(format, s)
	d.Date = t.Format(format)

	return
}
