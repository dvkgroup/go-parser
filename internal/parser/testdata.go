package parser

const (
	q1 = `
{
  "@context" : "https://schema.org/",
  "@type" : "JobPosting",
  "datePosted" : "2023-04-20",
  "title" : "Golang разработчик",
  "description" : "<p>Навыки: Golang. Специализации: Бэкенд разработчик.</p><p>В настоящее время находимся в поиске разработчика Golang и Solidity на новые проекты Иннотех.<br></p>\n<p><strong>Проект: </strong>высоконагруженная геораспределенная система обработки транзакций.</p>\n<p><strong>Наш стек: </strong>Golang, Ethereum, Quorum, Крипто про<br></p><ul></ul>\n<ul></ul>\n<p><strong>Наши задачи: </strong></p>\n<ul><li>разработка высоконагруженных приложений для финансового сектора;</li><li>реализация сервисов корпоративной блокчейн-сети;</li><li>поддержка и оптимизация существующего кода;</li><li>участие в нагрузочном тестировании;</li><li>развитие системы генерации нагрузки;</li><li>участие в проектировании архитектуры приложений;</li><li>взаимодействие с командой (разработчики, аналитики, архитекторы).</li></ul>\n<p><strong>Какие знания и навыки для нас важны:</strong></p>\n<ul><li>навыки использования Go;</li><li>опыт работы с блокчейн платформами (желательно Ethereum);</li><li>опыт написания смарт контрактов (желательно Solidity);</li><li>желателен опыт работы с криптографией (электронная подпись, шифрование);</li><li>желателен опыт оптимизации производительности систем распределенного реестра.</li></ul><ul><li>гибкий график работы — в офисе, удаленно или в гибридном формате</li><li>поддержка новичков по программе наставничества Buddy</li><li>ДМС с возможностью подключения близких родственников</li><li>сервис психологической поддержки, ведь нам очень важно, как ты себя чувствуешь</li><li>корпоративный кафетерий льгот с возможностью компенсации билетов и отелей в отпуске, абонементов в спортзал, спортинвентаря, билетов в театр и многого другого</li><li>насыщенная инножизнь: лекции и мастер-классы от известных спикеров, тимбилдинги, корпоративная библиотека и многое другое</li><li>скидки и купоны от сотен компаний-партнёров на единой платформе</li><li>маркетплейс мерча с собственной корпоративной валютой, которая начисляется за участие в активностях</li><li>дополнительные выходные в дни радостных (и не только) событий</li><li>дополнительный оплачиваемый отпуск (3 календарных дня)</li><li>широкие возможности внутреннего и внешнего обучения</li><li>необходимая техника и софт для работы</li><li>конкурентная зарплата по итогам интервью, наличие премиальной составляющей</li></ul>",
  "identifier": {
    "@type": "PropertyValue",
    "name": "Иннотех",
    "value": "1000123418"
  },
  "validThrough" : "2023-05-20",
  "hiringOrganization" : {
    "@type" : "Organization",
    "name" : "Иннотех",
      "logo" : "https://habrastorage.org/getpro/moikrug/uploads/company/100/007/328/7/logo/72954be0a36f28d596fa8c4612d4dc0d.png",
    "sameAs" : "https://inno.tech"
  },
    "jobLocation": {
      "@type": "Place",
      "address": {
        "@type": "PostalAddress",
        "streetAddress": "ул. Луговая, д. 4, корп. 5",
        "addressLocality": "Москва",
        "addressCountry": {
          "@type": "Country",
          "name": "Россия"
        }
      }
     },
  "jobLocationType": "TELECOMMUTE",
  "employmentType": "FULL_TIME"
}`
	q2 = `
{
  "@context" : "https://schema.org/",
  "@type" : "JobPosting",
  "datePosted" : "2023-05-14",
  "title" : "Разработчик Golang",
  "description" : "<p>Навыки: Golang, C++. Квалификация: Middle. Специализации: Бэкенд разработчик.</p><p><strong>Вакансия открыта под задачи </strong><strong>Topcon</strong> <strong>Positioning</strong> <strong>Systems</strong><strong> - </strong> является ведущим мировым разработчиком и производителем оборудования для точного позиционирования. Компания предлагает широчайший выбор инновационных по точности ГНСС систем, лазерных, оптических и других геодезических инструментов, а также систем управления строительной техникой.</p>\n<p><strong>Проект:</strong> Менеджмент машин и участков строительства. Передача данных между машинами и панелью управления. Возможность создавать проекцию на карте, по которой машина выполняет работу.</p>\n<p><strong>Задачи на проекте:</strong> Задача будет заключаться в построении, поддержке и тестировании внутренних микросервисных процессов, которые являются полиглотами — такими разными языками. Доминирующим языком является <strong>Golang</strong><strong>,</strong> но есть также <strong>C</strong><strong>++,</strong> Python и Java. В идеале опыт работы с Golang предпочтительнее, C++ будет следующим по полезности. Большинство разработчиков знают Python, и его легко освоить. </p>\n<p><strong>Требования</strong>: </p>\n<ul>\n <li>Умение работать в команде в условиях коллективного владения      кодом, аккуратность, ответственность, широкий кругозор</li>\n <li>Нацеленность на долгосрочное сотрудничество</li>\n <li>Высшее техническое образование</li>\n <li><strong>Разговорный уровень английского языка</strong> (для общения на      технические темы и документирования работы. Полностью англоязычная      команда)</li>\n <li>Глубокое      понимание разработки облачных сервисов и микросервисов</li>\n <li>Опыт      работы в Agile среде</li>\n <li>Опыт      создания веб-сервисов</li>\n <li>Опыт      работы в среде Polyglot</li>\n <li>Использование      баз данных SQL</li>\n <li>Отличные      навыки решения проблем, математические и коммуникативные навыки</li>\n</ul>\n<p><strong>Желательно</strong>:</p>\n<ul>\n <li>Использование      баз данных как SQL, так и NoSQL</li>\n <li>Опыт      использования Cassandra</li>\n <li>Знакомство      с Amazon Web Services (AWS)</li>\n <li>Опыт      разработки кода на Go-Lang, Python, C++, Ansible</li>\n</ul>\n<p><strong>Обязанности</strong>: </p>\n<ul>\n <li>Проектирование и      разработка распределенных облачных сервисов</li>\n <li>Разработать и провести      тестирование для подтверждения соответствия проекта/реализации      установленным требованиям</li>\n <li>Поддерживать связь с      различными заинтересованными сторонами и инженерными группами для сбора      требований</li>\n <li>Развивать и укреплять      отношения с внешними заказчиками и/или инженерными группами</li>\n</ul>\n<p><strong>Условия</strong>:</p>\n<ul><li>       Готовность работать удаленно по австралийскому времени (Австралия, Брисбен). <strong>Ищем кандидатов на Дальнем Востоке или в Сибири, возможна работа за пределами РФ но в тех же соседних часовых поясах относительно Австралии (не более 2-3ч разницы)</strong></li><li>       Возможность профессионального и карьерного роста в компании, возможность поучаствовать в разных проектах;</li><li>       Опыт работы в команде профессионалов;</li><li>       Уровень заработной платы обсуждается индивидуально</li></ul>",
  "identifier": {
    "@type": "PropertyValue",
    "name": "Bell Integrator",
    "value": "1000123086"
  },
  "validThrough" : "2023-06-13",
  "hiringOrganization" : {
    "@type" : "Organization",
    "name" : "Bell Integrator",
      "logo" : "https://habrastorage.org/getpro/moikrug/uploads/company/431/936/411/logo/2dd003e4f8c3fcb5dbc6ceb9876c3c40.png",
    "sameAs" : "http://www.bellintegrator.ru"
  },
  "jobLocation": [
      {
        "@type": "Place",
        "address": "Барнаул"
      },
      {
        "@type": "Place",
        "address": "Новосибирск"
      },
      {
        "@type": "Place",
        "address": "Хабаровск"
      }
  ],
  "employmentType": "FULL_TIME"
}`
)
