package repository

import (
	"errors"
	"gitlab.com/dvkgroup/go-parser/internal/entity"
	"strconv"
)

type VacancyStorage struct {
	vacancies map[int64]entity.Vacancy
}

func NewVacancyRepository() *VacancyStorage {
	return &VacancyStorage{vacancies: make(map[int64]entity.Vacancy)}
}

func (r VacancyStorage) Create(vacancy entity.Vacancy) (entity.Vacancy, error) {
	id, err := strconv.ParseInt(vacancy.Identifier.Value, 10, 64) // получаем ID
	if err != nil {
		return entity.Vacancy{}, err
	}

	// если id нет, то добавляем
	if _, ok := r.vacancies[id]; !ok {
		r.vacancies[id] = vacancy
		return vacancy, nil
	}

	// id уже существует
	return r.vacancies[id], errors.New("vacancy id exists in sqlDB")

}

func (r VacancyStorage) GetByID(id int64) (entity.Vacancy, error) {
	// если id есть
	if v, ok := r.vacancies[id]; ok {
		return v, nil
	}

	// id если нет
	return entity.Vacancy{}, errors.New("id not found")

}

func (r VacancyStorage) GetList() []entity.Vacancy {
	res := make([]entity.Vacancy, 0, len(r.vacancies))

	// map to slice
	for i := range r.vacancies {
		res = append(res, r.vacancies[i])
	}

	return res
}

func (r VacancyStorage) Delete(id int64) error {
	if _, ok := r.vacancies[id]; ok {
		delete(r.vacancies, id)
		return nil
	}

	return errors.New("id not found")
}
