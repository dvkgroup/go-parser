package sqlDB

import (
	"errors"
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"gitlab.com/dvkgroup/go-parser/internal/entity"
)

type VacancyStorage struct {
	db *sqlx.DB
}

func NewVacancyStorage(db *sqlx.DB) *VacancyStorage {
	return &VacancyStorage{db: db}
}

func (r VacancyStorage) Create(vacancy entity.Vacancy) (entity.Vacancy, error) {
	query := `INSERT INTO vacancies (id, title, description, identifier_type, identifier_name, 
                       				employment_type, job_location, date_posted, valid_through) 
			  VALUES (:identifier.id, :title, :description, :identifier.type, :identifier.name, 
			          :employment_type, :job_location.address, :date_posted.date, :valid_through.date)`

	_, err := r.db.NamedExec(query, vacancy)
	if err != nil {
		return entity.Vacancy{}, errors.New(fmt.Sprintf("id %s, error: %s", vacancy.Identifier.Value, err.Error()))
	}

	return vacancy, nil
}

func (r VacancyStorage) Read(id int64) (entity.Vacancy, error) {
	var vacancy entity.Vacancy

	stmt, err := r.db.Preparex(`SELECT id as "identifier.id", title, description, 
        							identifier_type as "identifier.type", identifier_name as "identifier.name", 
        							employment_type, job_location as "job_location.address", 
        							date_posted as "date_posted.date", valid_through as "valid_through.date"
									FROM vacancies WHERE id=$1`)
	if err != nil {
		return entity.Vacancy{}, err
	}

	err = stmt.Get(&vacancy, id)
	if err != nil {
		return entity.Vacancy{}, err
	}

	return vacancy, nil
}

func (r VacancyStorage) List() []entity.Vacancy {
	var res []entity.Vacancy

	err := r.db.Select(&res, `SELECT id as "identifier.id", title, description, 
									identifier_type as "identifier.type", identifier_name as "identifier.name",
									employment_type, job_location as "job_location.address",
									date_posted as "date_posted.date", valid_through as "valid_through.date"
									FROM vacancies`)
	if err != nil {
		return nil

	}

	return res
}

func (r VacancyStorage) Delete(id int64) error {
	var err error

	res, err := r.db.Exec(`DELETE FROM vacancies WHERE id=$1`, id)
	if err != nil {
		return err
	}

	count, err := res.RowsAffected()
	if err != nil {
		return err
	}

	if count == 0 {
		return errors.New("id not found")
	}

	return err
}

func (r VacancyStorage) Update(vacancy entity.Vacancy) (entity.Vacancy, error) {
	query := `UPDATE vacancies SET title=:title, description=:description, identifier_type=:identifier.type,
                     identifier_name=:identifier.name, employment_type=:employment_type, 
                     job_location=:job_location.address, date_posted=:date_posted.date, 
                     valid_through=:valid_through.date 
 			  WHERE id=:identifier.id`

	_, err := r.db.NamedExec(query, vacancy)
	if err != nil {
		return entity.Vacancy{}, errors.New(fmt.Sprintf("id %s, error: %s", vacancy.Identifier.Value, err.Error()))
	}

	return vacancy, nil
}
