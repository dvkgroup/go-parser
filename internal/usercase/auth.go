package usercase

import (
	"gitlab.com/dvkgroup/go-parser/internal/entity/auth"
	"gitlab.com/dvkgroup/go-parser/internal/restapi"
)

type AuthService struct {
	c *restapi.Client
}

func NewAuthService(c *restapi.Client) *UserService {
	return &UserService{c: c}
}

func (s *UserService) Register(token string, in auth.RegisterRequest) (auth.RegisterResponse, error) {
	req, err := s.c.NewRequest("POST", "/api/1/auth/register", token, in)
	if err != nil {
		return auth.RegisterResponse{}, err
	}
	var data auth.RegisterResponse
	_, err = s.c.Do(req, &data)

	return data, nil
}

func (s *UserService) Login(token string, in auth.LoginRequest) (auth.AuthResponse, error) {
	req, err := s.c.NewRequest("POST", "/api/1/auth/login", token, in)

	if err != nil {
		return auth.AuthResponse{}, err
	}
	var data auth.AuthResponse
	_, err = s.c.Do(req, &data)

	return data, err
}

func (s *UserService) Refresh(token string) (auth.AuthResponse, error) {
	req, err := s.c.NewRequest("POST", "/api/1/auth/refresh", token, nil)
	if err != nil {
		return auth.AuthResponse{}, err
	}
	var data auth.AuthResponse
	_, err = s.c.Do(req, &data)

	return data, nil
}

func (s *UserService) Verify(token string, in auth.VerifyRequest) (auth.VerifyResponse, error) {
	req, err := s.c.NewRequest("POST", "/api/1/auth/verify", token, in)
	if err != nil {
		return auth.VerifyResponse{}, err
	}
	var data auth.VerifyResponse
	_, err = s.c.Do(req, &data)

	return data, nil
}
