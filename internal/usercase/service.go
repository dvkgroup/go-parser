package usercase

import (
	"gitlab.com/dvkgroup/go-parser/internal/entity"
	"log"
)

type VacancyParser interface {
	Parse(string) ([]entity.Vacancy, error)
}

type VacancyRepository interface {
	Create(entity.Vacancy) (entity.Vacancy, error)
	Read(int64) (entity.Vacancy, error)
	List() []entity.Vacancy
	Delete(int64) error
	Update(vacancy entity.Vacancy) (entity.Vacancy, error)
}

type VacancyService struct {
	r VacancyRepository
	p VacancyParser
}

func NewVacancyService(r VacancyRepository, p VacancyParser) *VacancyService {
	return &VacancyService{r: r, p: p}
}

func (s *VacancyService) Search(query string) int {
	log.Println("parser start (query :", query, ")")
	vacancies, err := s.p.Parse(query)
	if err != nil {
		log.Println("Search (parse):", err)
		return 0
	}

	for i := range vacancies {
		_, err := s.r.Create(vacancies[i])
		if err != nil {
			log.Println("Search (create):", err)
		}
	}
	log.Println("parser.. ok!")

	return len(vacancies)
}

func (s *VacancyService) Create(vacancy entity.Vacancy) (entity.Vacancy, error) {
	return s.r.Create(vacancy)
}

func (s *VacancyService) GetByID(id int64) (entity.Vacancy, error) {
	return s.r.Read(id)
}

func (s *VacancyService) GetList() []entity.Vacancy {
	return s.r.List()
}

func (s *VacancyService) Delete(id int64) error {
	return s.r.Delete(id)
}

func (s *VacancyService) Update(vacancy entity.Vacancy) (entity.Vacancy, error) {
	return s.r.Update(vacancy)
}
