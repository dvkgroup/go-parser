package usercase

import (
	"gitlab.com/dvkgroup/go-parser/internal/entity/user"
	"gitlab.com/dvkgroup/go-parser/internal/restapi"
)

type UserService struct {
	c *restapi.Client
}

func NewUserService(c *restapi.Client) *UserService {
	return &UserService{c: c}
}

func (s *UserService) Profile(token string) (user.ProfileResponse, error) {
	req, err := s.c.NewRequest("GET", "/api/1/user/profile", token, nil)
	if err != nil {
		return user.ProfileResponse{}, err
	}
	var data user.ProfileResponse
	_, err = s.c.Do(req, &data)

	return data, err
}

func (s *UserService) ChangePassword(token string, cpr user.ChangePasswordRequest) (user.ChangePasswordResponse, error) {
	req, err := s.c.NewRequest("POST", "/api/1/user/profile/chpass", token, cpr)
	if err != nil {
		return user.ChangePasswordResponse{}, err
	}
	var data user.ChangePasswordResponse
	_, err = s.c.Do(req, &data)

	return data, nil
}
